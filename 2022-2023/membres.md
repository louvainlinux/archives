# Années 2022-2023

## Président : 

Nicolas Jeanmenne

## Vice-président

Tania Shafiei

## Tréso

Guillaume Jadin

## Internes

* Adrien Giot
* Alexandre Zenon : Devops
* Brieuc Dubois : Devops
* Martin Gyselinck : Responsable mail
* Alexandre Dewilde : Responsable communication
* André Magerat : Responsable patrimoine

## Externes

* Félix Gaudin : Responsable communication
* Théo Vanden Driessche : Devops
* Aymeric Wibo : Devops
* Emile Villette : Devops
* Aylin Defoy
* Clément Delzotti
* Vincent Higginson
